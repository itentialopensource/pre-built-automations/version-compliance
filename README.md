<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your pre-built name -->
# Version Compliance

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

This pre-built provides users with the ability to test whether their devices are compliant with a specific version or set of versions. This is important for users that have many devices across many different device groups and golden configuration trees. This pre-built is able to validate whether or not these devices have fallen out of compliance for a given version(s). This pre-built  works completely out-of-the-box and can work with any device connected to IAG.

This solution consists of the following:
* Operations Manager Automation (**Version Compliance**)
* JSON Form (**Version Compliance**)
* Jinja2 Template (**Version Compliance**)
* Workflow (**Version Compliance**)
* Workflow (**Version Compliance - Get Devices**)
* Workflow (**Version Compliance - Version Data**)
* Transformation (**version-compliance-ConcatDedupe**)
* Transformation (**version-compliance-ConnectionObject**)
* Transformation (**version-compliance-MakeVersionObject**)
* Transformation (**version-compliance-MergeVersionData**)
* Transformation (**version-compliance-Sources**)
* Transformation (**version-compliance-UnreachableDevice**)
* Pre-Built Automation (**Find Active Host**)


<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
  <img src="./images/output.png" alt="output" width="300px">
</td></tr></table>
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: Less than 5 minutes

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`

## Requirements

This pre-built requires the following:

<!-- Unordered list highlighting the requirements of the pre-built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Device Group containing device(s) that are onboarded onto IAG

## Features

The main benefits and features of the pre-built are outlined below.

<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
  * Generates a compliance report for all input devices and versions
  * Two modes of operation: Zero-Touch and Verbose
  * A formatted table to clearly view results in Verbose mode
  * Proper outcome variables so that the parent workflow can easily handle the command template results

## Future Enhancements

<!-- OPTIONAL - Mention if the pre-built will be enhanced with additional features on the road map -->
<!-- Ex.: This pre-built would support Cisco XR and F5 devices -->
* Add NSO platform support for device version information
* Add ability to parse golden configuration tree for devices

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 
* The pre-built can be installed from within Admin Essentials app. Simply search for the name of your desired pre-built and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED pre-built -->
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED pre-built -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

* Navigate to Operations Manager and select the "Version Compliance" automation.
* Select the manual trigger and fill in the JSON form with the desired device groups to monitor compliance for all the devices within those groups.
* Enter one or more pairings of Device Model and Device Version in the `Compliant Versions` table that you would like to validate the compliance.
* Navigate to active job to monitor progress.
